// 挂载
const Koa = require('koa');
const app = new Koa;

// 挂载
const router = require('koa-router')()
const bodyparser = require('koa-bodyparser')
app.use(router.routes())
app.use(router.allowedMethods())
app.use(bodyparser())

// 挂载
const json = require('koa-json')
app.use(json())

//引入路由文件
const banner = require('./router/index')

//注册路由中间件
app.use(banner.routes(), banner.allowedMethods())

// 全局异常处理
const abnormal = require('./utils/abnormal')
app.use(abnormal)

// 挂载
const mongoose = require('mongoose')

// 连接阿里云数据库
mongoose
  .connect('mongodb://root:Wang253547@dds-uf63bbe5340f51641573-pub.mongodb.rds.aliyuncs.com:3717,dds-uf63bbe5340f51642116-pub.mongodb.rds.aliyuncs.com:3717/node?replicaSet=mgset-63496652', {
    useNewUrlParser: true, useUnifiedTopology: true, authSource: 'admin'
  })
  .then((res) => {
    console.log('连接成功')
  })
  .catch((err) => {
    console.log('连接失败')
  })

// 3000 端口号
app.listen(3000)
console.log('http://localhost:3000/');