const { log } = console
//路由
const router = require('koa-router')()
//注册模型
const Account = require('../models/index.js')
// const Application = require('koa')
const initdata = require('../utils/init')

//引入token生成函数
const { jwtSign } = require('../utils/token')


// 注册的路由
router.post('/register', async (ctx, next) => {
  await next()
  let { username, password, openid } = ctx.request.body
  const user = new Account({
    username: username,
    password: password,
    openid: openid
  })
  // 校验
  if (username === '' | password === '') {
    new initdata(ctx).tips('用户名和密码不能为空', 202)
    return false
  }
  if (username === undefined | password === undefined) {
    new initdata(ctx).tips('参数填写不正确', 400)
    return false
  }
  if (username !== 'admin' || password !== 'admin') {
    // 用户名校验
    let phone = /^1[3456789]\d{9}$/
    if (!phone.test(username)) {
      new initdata(ctx).tips('手机号码不正确', 202)
      return false
    }
    // 密码校验
    let reg = /^(?![\d]+$)(?![a-zA-Z]+$)(?![^\da-zA-Z]+$).{6,20}$/
    if (!reg.test(password)) {
      new initdata(ctx).tips('密码必须由6~20位数字和字母的组合', 202)
      return false
    }
  }
  // 存储到数据库
  await user.save()
    .then((res) => {
      new initdata(ctx, '注册成功').listing()
    }).catch((res => {
      new initdata(ctx).tips('注册失败', 500)
    }))
})

// 增 <Schema模型>.create(object)
router.post('/increase', async (ctx, next) => {
  await next()
  let { username, password, openid } = ctx.request.body
  const user = new Account({
    username: username,
    password: password,
    openid: openid
  })
  // 校验
  if (username === '' | password === '') {
    new initdata(ctx).tips('用户名和密码不能为空', 202)
    return false
  }
  if (username === undefined | password === undefined) {
    new initdata(ctx).tips('参数填写不正确', 400)
    return false
  }
  // 用户名校验
  let phone = /^1[3456789]\d{9}$/
  if (!phone.test(username)) {
    new initdata(ctx).tips('手机号码不正确', 202)
    return false
  }
  // 密码校验
  let reg = /^(?![\d]+$)(?![a-zA-Z]+$)(?![^\da-zA-Z]+$).{6,20}$/
  if (!reg.test(password)) {
    new initdata(ctx).tips('密码必须由6~20位数字和字母的组合', 202)
    return false
  }
  try {
    let result = await Account.create(user)
    ctx.body = {
      code: 200,
      msg: '添加新用户成功'
    }
  } catch (error) {
    ctx.body = {
      code: 400,
      msg: '添加新用户失败'
    }
  }
})

// 删 <Schema模型>.deleteOne(object)
router.post('/delete', async (ctx, next) => {
  await next()
  let _id = ctx.request.body.openid
  try {
    let result = await Account.deleteOne({ openid: _id })
    ctx.body = {
      code: 200,
      msg: '删除用户成功'
    }
  } catch (error) {
    ctx.body = {
      code: 400,
      msg: '操作失败'
    }
  }
})

// 改 <Schema模型>.updateOne(<filter筛选条件>,object)
router.post('/change', async (ctx, next) => {
  await next()
  let openid = ctx.request.body.openid
  const user = {
    username: ctx.request.body.username,
    password: ctx.request.body.password,
  }
  try {
    let result = await Account.updateOne({ "openid": openid }, user);
    ctx.body = {
      code: 200,
      msg: "更新用户成功"
    }
  } catch (error) {
    ctx.body = {
      code: 400,
      msg: "更新用户失败"
    }
  }
})

// 查 <Schema模型>.find(object) / <Schema模型>.findOne(object)

// 登入接口
router.post('/login', async (ctx, next) => {
  await next()
  log(ctx.request.body)
  let { username, password } = ctx.request.body
  // 校验
  if (username === '' | password === '') {
    new initdata(ctx).tips('用户名和密码不能为空', 202)
    return false
  }
  if (username === undefined | password === undefined) {
    new initdata(ctx).tips('参数填写不正确', 400)
    return false
  }
  if (username !== 'admin' || password !== 'admin') {
    // 用户名校验
    let phone = /^1[3456789]\d{9}$/
    if (!phone.test(username)) {
      new initdata(ctx).tips('手机号码不正确', 202)
      return false
    }
    // 密码校验
    let reg = /^(?![\d]+$)(?![a-zA-Z]+$)(?![^\da-zA-Z]+$).{6,20}$/
    if (!reg.test(password)) {
      new initdata(ctx).tips('密码必须由6~20位数字和字母的组合', 202)
      return false
    }
  }
  // 查询集合
  let listdata = await Account.find({})
  // 用引入的jwtSign方法生成token并返回
  const token = jwtSign({})
  if (listdata.length === 0) {
    new initdata(ctx).tips('账号或密码错误', 202)
  } else {
    ctx.body = {
      code: 200,
      data: {
        pageItems: token,
        total: 1
      },
      message: 'success'
    }
  }
})

// router.post('/login', async (ctx, next) => {
//   await next()
//   log(ctx.request.body)
//   let { username, password } = ctx.request.body
//   // 校验
//   if (username === '' | password === '') {
//     new initdata(ctx).tips('用户名和密码不能为空', 202)
//     return false
//   }
//   if (username === undefined | password === undefined) {
//     new initdata(ctx).tips('参数填写不正确', 400)
//     return false
//   }
//   if (username !== 'admin' || password !== 'admin') {
//     // 用户名校验
//     let phone = /^1[3456789]\d{9}$/
//     if (!phone.test(username)) {
//       new initdata(ctx).tips('手机号码不正确', 202)
//       return false
//     }
//     // 密码校验
//     let reg = /^(?![\d]+$)(?![a-zA-Z]+$)(?![^\da-zA-Z]+$).{6,20}$/
//     if (!reg.test(password)) {
//       new initdata(ctx).tips('密码必须由6~20位数字和字母的组合', 202)
//       return false
//     }
//   }
//   // 查询集合
//   let listdata = await Account.find({})
//   // 用引入的jwtSign方法生成token并返回
//   const token = jwtSign({})
//   console.log('token: ', token);
//   if (listdata.length === 0) {
//     new initdata(ctx).tips('账号或密码错误', 202)
//   } else {
//     new initdata(ctx, token, 'success', listdata, 200).listing()
//   }
// })


module.exports = router