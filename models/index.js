const mongoose = require('mongoose')
const Schemas = mongoose.Schema

const NameSchema = new Schemas({
  //注册对象模型 type：数据类型；require：必填
  username: {
    type: String,
    require: true
  },
  password: {
    type: String,
    require: true
  },
  openid: {
    type: String,
    require: true
  }
},
  {
    versionKey: false
  }
)

module.exports = Account = mongoose.model('accountdata', NameSchema)