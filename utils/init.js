// 处理响应
class initdata {
  constructor(ctx, message = 'success', data = [], code = 200) {
    this.ctx = ctx
    this.message = message
    this.data = data
    this.code = code
  }

  //200
  listing() {
    this.ctx.body = {
      code: this.code,
      message: this.message,
      data: this.data
    }
    this.ctx.status = this.code
  }

  // 参数不对
  tips(tipmessage, codes) {
    this.ctx.body = {
      message: tipmessage
    }
    this.ctx.status = codes
  }
}
module.exports = initdata