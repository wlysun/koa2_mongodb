const result = require('./resultdata')

let abnormal = async (ctx, next) => {
  try {
    await next()
  } catch (err) {
    const isresult = err instanceof result
    if (isresult) {
      console.log('1111');
      ctx.body = {
        msg: err.msg
      }
      ctx.status = err.code
    } else {
      ctx.body = {
        msg: '服务器发生错误'
      }
      ctx.status = 500
    }
  }
}

module.exports = abnormal